Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Contact: Mark Overmeer <perl@overmeer.net>
Upstream-Name: XML-Compile-WSDL11
Source: https://metacpan.org/release/XML-Compile-WSDL11
Disclaimer: This package is not part of the Debian operating system.
 The libxml-compile-wsdl11-perl source and binary Debian packages are part of
 the "non-free" area in our archive. The packages in this area are not part of
 the Debian system, although they have been configured for use with Debian. We
 nevertheless support the use of the libxml-compile-wsdl11-perl source and
 binary Debian packages and provide infrastructure for non-free packages (such
 as our bug tracking system and mailing lists).
 .
 This package is part of the "non-free" area of the Debian archive because the
 following files are released under a license which does not permit
 modification:
 .
 - lib/XML/Compile/WSDL11/xsd/wsdl.xsd
 - lib/XML/Compile/WSDL11/xsd/wsdl-http.xsd
 - lib/XML/Compile/WSDL11/xsd/wsdl-mime.xsd

Files: *
Copyright: 2014-2021, Mark Overmeer <perl@overmeer.net>
License: Artistic or GPL-1+

Files: bin/wsdl-explain.pl
       bin/xsd-explain.pl
Copyright: 2015, Wesley Schwengle
License: Artistic or GPL-1+

Files: debian/*
Copyright: 2016, Nick Morrott <knowledgejunkie@gmail.com>
License: Artistic or GPL-1+

Files: lib/XML/Compile/WSDL11/xsd/wsdl.xsd
       lib/XML/Compile/WSDL11/xsd/wsdl-http.xsd
       lib/XML/Compile/WSDL11/xsd/wsdl-mime.xsd
Copyright: 2001-2005, International Business Machines Corporation and Microsoft Corporation
License: License-for-WSDL-Schema-Files
Comment: Files released under this license do not permit modification

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.

License: License-for-WSDL-Schema-Files
 The Authors grant permission to copy and distribute the WSDL Schema
 Files in any medium without fee or royalty as long as this notice and
 license are distributed with them.  The originals of these files can
 be located at:
 .
 http://schemas.xmlsoap.org/wsdl/soap/2003-02-11.xsd
 .
 THESE SCHEMA FILES ARE PROVIDED "AS IS," AND THE AUTHORS MAKE NO REPRESENTATIONS
 OR WARRANTIES, EXPRESS OR IMPLIED, REGARDING THESE FILES, INCLUDING, BUT NOT
 LIMITED TO, WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE,
 NON-INFRINGEMENT OR TITLE.  THE AUTHORS WILL NOT BE LIABLE FOR ANY DIRECT,
 INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF OR
 RELATING TO ANY USE OR DISTRIBUTION OF THESE FILES.
 .
 The name and trademarks of the Authors may NOT be used in any manner,
 including advertising or publicity pertaining to these files or any program
 or service that uses these files, written prior permission.  Title to copyright
 in these files will at all times remain with the Authors.
 .
 No other rights are granted by implication, estoppel or otherwise.
